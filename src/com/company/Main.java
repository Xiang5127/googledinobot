package com.company;
import java.awt.*;
import java.awt.event.KeyEvent;

public class Main {

    public static void main(String[] args) throws AWTException, InterruptedException {
        //Robot dino instance created
        Robot dino = new Robot();

        //Infinite game play mode
        while(true) {
            //get pixel's coordinates
            //stroke "Space" key if occur object
            Color obstacle = new Color(83, 83, 83);
            if(dino.getPixelColor(553, 226).equals(obstacle)){
                System.out.println("Up!");
                dino.keyPress(KeyEvent.VK_UP);
                dino.keyRelease(KeyEvent.VK_UP);
            }else if( dino.getPixelColor(588, 221).equals(obstacle)){
                System.out.println("Bird!");
                dino.keyPress(KeyEvent.VK_DOWN);
                dino.keyRelease(KeyEvent.VK_DOWN);
            }
        }
    }
}
