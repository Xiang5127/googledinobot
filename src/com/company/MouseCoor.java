package com.company;
import java.awt.*;

public class MouseCoor {
    private int x, y;

    public void start() {
        //Infinite checks for mouse movement
        while(true) {
            if(MouseInfo.getPointerInfo().getLocation().x != x){
                x = MouseInfo.getPointerInfo().getLocation().x;
                //printing coordinates out
                System.out.println("X: " + x);
            }
            if(MouseInfo.getPointerInfo().getLocation().y != y){
                y = MouseInfo.getPointerInfo().getLocation().y;
                //printing coordinates out
                System.out.println("Y: " + y);
            }
        }
    }
    public static void main(String []args){
        new MouseCoor().start();
    }
}
